What is ``buildbox-casd``?
==========================

``buildbox-casd`` is a local cache and proxy and for
``ContentAddressableStorage`` services described by Bazel's
`Remote Execution API`_.

.. _Remote Execution API: https://docs.google.com/document/d/1AaGk7fOPByEvpAbqeXIyE8HX_A3_axxNnvroblTZ_6s/edit#heading=h.ole76l21af90


Usage
=====

  buildbox-casd [OPTIONS] LOCAL_CACHE

This starts a daemon storing local cached data at LOCAL_CACHE.

* --instance=INSTANCE

  Instance name to pass to the Remote Workers API server. Without this option,
  ``buildbox-worker`` will pass an empty string as the instance name.

* --verbose

  Output additional information to stdout that may be useful when debugging.
